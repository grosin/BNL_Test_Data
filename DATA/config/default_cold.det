#
# Default Configuration file for ABCD3T module
#
# P.W.Phillips 18.01.2002
#
# Here is a brief explanation of the variables that have to be set:
#
# First row
#  Required for all modules
#   Link0   enable MUSTARD channel 0 (associated to first hybrid)
#   Link1   enable MUSTARD channel 1 (associated to second hybrid)
#   Oddity  How is the top address line of the chips bonded?
#            -1 - follows SELECT line
#             0 - bonded low => hybrid is even
#             1 - unbonded   => hybrid is odd
#   Chipset 3 = ABCD (not actually used yet)
#   DTM     "Enable Data Taking Mode" - set 1 here!!
#  Extras required for Liverpool Forward Support Card
#   (see http://hep.ph.liv.ac.uk/~ashley/support.html)
#   SCmode  Support Card readout mode (0-3)
#   bpm_dr  DORIC bpm drive current (0-4)
#   vdac0   threshold for data receiver 0 (0-1023)
#   vdac1   threshold for data receiver 1 (0-1023)
#
# Second row
#  Required for all modules
#   Select use primary or redundant clock and control
#   Vdet    detector bias voltage
#   Idet    detector bias current trip limit (remember to allow for charging currents)
#   Vcc
#   Icc     current warning limit (no effect)
#   Vdd
#   Idd     current warning limit (no effect)
#   Vi1     - redundant -
#   iVi1    - redundant -
#   vled0   (Vvcsel0)
#   Iled0   (Ivcsel0) current warning limit (no effect)
#   vled0   (Vvcsel0)
#   Iled0   (Ivcsel0) current warning limit (no effect)
#   Vpin
#   Ramp    HV ramp rate [1(fast) - 4(slow)] (assumes SCTHV)
#
# One row per chip, preceeded by "Chip n" tag
#  Comp.    Compression mode (0-3)
#  Act.     Is chip active? (must be active to participate in scans)
#  Cal_m    Calibration Mode - select strobed cal line (0-3)
#  T_range  TrimDAC range (0-3)
#  Mask_r   Mask bit - readout contents of mask register (0/1)
#  Edge     Edge Detect bit (0/1)
#  Acc.     Accumulate bit (0/1)
#  Del.     Strobe Delay      (0-63)
#  Vth      Threshold Voltage (0-637.5 mV)
#  Vcal     Calibration level (0-159.375 mV)
#  FEShp.   FE Shaper Current (0-37.2 microA)
#  FEBias   FE Bias Current   (0-285.2 microA)
#  Role     Role of chip:
#             0 - missing
#             1 - dead    (bypass this chip)
#             2 - end
#             3 - master
#             4 - slave
#             5 - lonely  (master + end)
#
#
Module : Link0  Link1  Oddity Chipset DTM SCmode bpm_dr vdac0 vdac1
           1      1      -1      4     1    0      3     650   650
	 Select  Vdet  Idet  Vcc  Icc  Vdd  Idd  Vi1  iVi1 Vled0 Iled0 Vled1 Iled1 Vpin Ramp
	    0    200.  100.  3.5 1000. 4.0  600.  0.  10.    6.   10.    6.   10.   6.   2

Chip 0 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     3  
Chip 1 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 2 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 3 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 4 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 5 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     2  
Chip 6 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     3  
Chip 7 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 8 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 9 : Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 10: Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     4  
Chip 11: Comp. Act. Cal_m Trim_r Mask_r Edge Acc. Del. Vth Vcal FEShp. FEBias Role
           1     1     0     0     0    0    0   12   500. 15. 30.0   220.0     2  
#
# optional extensions
#
# list of masked channels, preceeded by tag "Mask"
#
# 1536* TrimDAC settings (integers), preceeded by tag "Trim"
#
