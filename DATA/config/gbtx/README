GBTX configuration
==================

The GBTX configuration consists of 366 bytes, encoding several registers,
some of which are triplicated in different portions of the address space.

There seem to be two common configuration formats for the GBTX.

1) List of bytes in hex (one per line).

2) XML file, which encodes both the logical register values and their
mapping to the configuration space.

In this directory are some example configurations in each of the two formats.
A python script is provided (via Stephanie Sullivan) to read the xml
configuration and output the raw format.

Example usage:

> .L gbtx.cpp
> gbt_config = readGBTXConfigBytes("config/gbtx/MinimalConfig.txt");
> writeGBTXData(0, 1, gbt_config);

> writeGBTXRegister(0, 1, 280, 0x55); // testOutputSelect register

> read_config = readGBTXRegisters(0, 1);
> printGBTXConfigRaw(read_config);


Python script:

> import readXML
> data = readXMLfromCern("Loopback_test.xml")
> # Print data in same format as txt file above
> print("\n".join("%02x" % d for d in data))
> # Read xml file and use it to find logical registers
> interpretUsingXML("Loopback_test.xml", data)
