#
# Default Configuration file for an SCT module
# 
# P.W.Phillips 18.01.2002
# B Gallop      7.10.2008 - First ABCN version
#
# The general structure of this file is as follows. There are a series of entries identified
#  by a header string at the beginning of the line which determine the contents of the
#  following line(s)
# Currently, only 4 characters at the beginning of the line are read to identify the header type
# Numbers on the following lines are parsed by scanf, which stops at the first non-numerical argument
# Lines starting with # and those starting with whitespace with no numbers are ignored
#
# The first header starts with "Module"
#  This is followed by two rows of numbers:
#
# First row
#  Required for all modules
#   Link0   enable MUSTARD channel 0 (associated to first hybrid)
#   Link1   enable MUSTARD channel 1 (associated to second hybrid)
#   Oddity  How is the top address line of the chips bonded?
#            -1 - follows SELECT line
#             0 - bonded low => hybrid is even
#             1 - unbonded   => hybrid is odd
#   Chipset  3 = ABCD, 4 = ABCDT, 6 = ABCN (only partially used as yet)
#   DTM     "Enable Data Taking Mode" - set 1 here!! (otherwise stays in config readback mode)
#  Extras required for Liverpool Forward Support Card
#   (see http://hep.ph.liv.ac.uk/~ashley/support.html)
#   SCmode  Support Card readout mode (0-3)
#   bpm_dr  DORIC bpm drive current (0-4)
#   vdac0   threshold for data receiver 0 (0-1023)
#   vdac1   threshold for data receiver 1 (0-1023)
#
# Second row
#  Required for all modules
#   Select  use primary or redundant clock and control
#   Vdet    detector bias voltage
#   Idet    detector bias current trip limit (remember to allow for charging currents)
#   Vcc
#   Icc     current warning limit (no effect)
#   Vdd
#   Idd     current warning limit (no effect)
#   Vi1     - redundant -
#   iVi1    - redundant -
#   vled0   (Vvcsel0)
#   Iled0   (Ivcsel0) current warning limit (no effect)
#   vled0   (Vvcsel0)
#   Iled0   (Ivcsel0) current warning limit (no effect)
#   Vpin
#   Ramp    HV ramp rate [1(fast) - 4(slow)] (assumes SCTHV)
#
#
# A chip entry starts with a row "Chip n" tag where n is the chip address
# The following row contains these fields:
#  Act.      Is chip active? (must be active to participate in scans)
#  MasterB   Not master ? (spec inconsistent)
#  End       End chip
#  Comp.     Compression mode (0-3)
#  Cal_m     Calibration Mode - select strobed cal line (0-3)
#  T_range   TrimDAC range (0-7)
#  Mask_r    Mask bit - readout contents of mask register (0/1)
#  Edge      Edge Detect bit (0/1)
#  Clock/2   Feed through bit
#  Offset    DAC offset??? (0-3)
#  DriveU    Current to drive up                     ## (0-31) no units
#  DriveD    Current to drive down                   ## (0-31) no units
#
##
# 
# This may be followed by a header "Delay"
#  with the following fields on the next row:
#
#  Del.     Strobe Delay                             ## (0-63)
#  DelStep. Step for Strobe Delay                    ## 0: no step  1: 0.8ns   2: 1.0ns  3: 1.3ns
#  Vth+     Positive Threshold Voltage               ## (0-816mV 3.2 per step)
#  Vth-     Negative Threshold Voltage               ## (0-816mV 3.2 per step)
#  Vcal     Calibration level                        ## (0-204 mV 0.8 per step)  (0-10.2fC)
#
#  L1Delay  Delay for L1                             ## (0-255)
#  L1Comm   Use command stream for L1A
#  BISTPipe Activate pipeline self-test
#  BISTRand Activate derandomizer self-test
#
##
# 
# There may be a header "Bias" with the following fields on the next row:
#
#  Shaper    Shaper bias current                 ## (5.4-13.2 microA 0.25 per step)
#  ShapFbck  Shaper feedback bias current        ## (6.3-13.2 microA 0.22 per step)
#
#  Preamp    Input transistor bias current       ## (81-198 microA 3.65 per step)
#  PreBuff   Preamplifier buffer bias current    ## (5.4-13.2 microA 0.25 per step)
#  PreFbck   Preamplifier feedback bias current  ## (0.18-0.11 microA 0.03 per step)
#
#  DiffBias  Differential Bias                   ## (18-44 microA 0.84 per step)
#  CompBias  Comparator Bias                     ## (18-44 microA 0.84 per step)
#
#

Module : Link0  Link1  Oddity Chipset DTM SCmode bpm_dr vdac0 vdac1
           1      0      -1      6     1    0      3     512   512
	 Select  Vdet  Idet  Vcc  Icc  Vdd  Idd  Vi1  iVi1 Vled0 Iled0 Vled1 Iled1 Vpin Ramp
	    0    200.  100.  3.5 1000. 4.0  600.  0.  10.    6.   10.    6.   10.   6.   2

		# 64 = 0x40  -> 0x020
		# 65 = 0x41  -> 0x820
		# 68 = 0x44  -> 0x220
		# 69 = 0x45  -> 0xa20
		# 70 = 0x46  -> 0x620
		# 71 = 0x47  -> 0xe20
		# 73 = 0x49  -> 0x920
		
Chip 0 :  Act. MasterB End  Comp. Cal_m T_range Mask_r Edge Clock/2 Offset Cal+ Sig+ DownNotUp DriveU DriveD
           1    0       1    0     0     0       0      0    1       0      0    0    0         8      8
Bias : Shaper ShapFbck Preamp PreBuff PreFbck DiffBias CompBias
       9.4    9.82     139.4  9.4     0.3     31.44    31.44
Delay : Del. DelStep. Vth+ Vth- VCal L1Delay L1Comm BISTPipe BISTRand
        43   2        20   0    50   128     1      0        0

#
# optional extensions
#
# list of masked channels (indexed by chip * 128 + channel), one per line, preceeded by tag "Mask"
#
# 1536* TrimDAC settings (integers), one per line, preceeded by tag "Trim"
#
# Response curve data, preceeded by tag "RC_Data", one of
#  chip cal_fac
#  chip method function p0 p1 p2
#  chip method function p0 p1 p2 cal_fac
#
Mask
0 0
