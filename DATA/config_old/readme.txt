config/readme.txt

Please note that SCTDAQ uses configuration information from 
the directory sctvar\config.  These files are provided as 
examples to be copied and modified for your own use.

Detector Configuration files "mymodule.det"
  including the default file "default.det".
  See the included files for further details.

Module mask files "mymodule.trim"
  a list of channels to be masked, one per line.
  link 0 - channels   0 to  767
  link 1 - channels 768 to 1535
  (This information can now be optionally included in the 
  detector configuration file)

Module trim files "mymodule.trim"
  a list of 1536 floating point numbers between 0 and 1.
  Each number is converted to the nearest matching fraction
  of the four bit trim DAC.
  (This information can now be optionally included in the 
  detector configuration file in integer format)

beta test config files "mymodule.btc"
  reference: http://www-ucjf.troja.mff.cuni.cz/~sct/tests/beta/

The slog.txt files (also in triggers) allow bit-by-bit configuration of trigger
patterns and are not restricted to use on slog. The ABC130 test vectors are also
stored in a similar format, see sctdaq/vectors/abc130.
